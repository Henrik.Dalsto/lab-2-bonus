package INF101.lab2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    private int itemsInFridge = 0;
    private final int capacity = 20;
    private final ArrayList<FridgeItem> fridge = new ArrayList<>(20);

    @Override
    public int nItemsInFridge() {
        return itemsInFridge;
    }

    @Override
    public int totalSize() {
        return capacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (itemsInFridge + 1 <= capacity) {
            itemsInFridge++;
            fridge.add(item);
            return true;
        }

        return false;
    }

    @Override
    public void takeOut(FridgeItem item) throws NoSuchElementException {
        if (fridge.contains(item)) {
            fridge.remove(item);
            itemsInFridge--;
        } else {
            throw new NoSuchElementException("Not found");
        }
    }

    @Override
    public void emptyFridge() {
        fridge.clear();
        itemsInFridge = 0;
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired = new ArrayList<>();
        for (FridgeItem item : fridge) {
            if (item.hasExpired()) {
                expired.add(item);
            }
        }

        for (FridgeItem item : expired) {
            fridge.remove(item);
            itemsInFridge--;
        }

        return expired;
    }
}
